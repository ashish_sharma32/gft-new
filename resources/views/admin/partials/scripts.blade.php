{!! Html::script('assets_admin/lib/jquery/jquery.min.js') !!}
{!! Html::script('assets_admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('assets_admin/lib/bootstrap/dist/js/bootstrap.min.js') !!}
{!! Html::script('assets_admin/js/rmv_entity.js') !!}
{!! Html::script('https://unpkg.com/sweetalert/dist/sweetalert.min.js') !!}
{!! Html::script('assets_admin/js/main.js') !!}
<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
});
</script>

